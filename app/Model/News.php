<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class News extends Model
{
    protected $guarded = [];

    const STATUS_DRAFT = 'Draft';
    const STATUS_PUBLISHED = 'Published';
}
