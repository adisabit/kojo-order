<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MasterPostalCode extends Model
{
    protected $table = 'master_postal_codes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];
}
