<?php

namespace App\Helper;

use App\Model\Order;

abstract class OrderHelper
{
    /**
     * Generate order ref
     *
     * @return string
     */
    public static function generateOrderRef(): string
    {
        $startDate = date("Y-m-01");
        $endDate = date("Y-m-t");

        $orders = Order::whereBetween('order_time', [$startDate, $endDate])->get();
        $totalOrder = count($orders);
        $currentOrder = sprintf("%02d", $totalOrder + 1);

        $orderRef = sprintf("%s-%s%s%s", $currentOrder, date("d"), date("m"), date('y'));

        return $orderRef;
    }

    /**
     * Map
     *
     * @param array $data
     * @param string $key
     * @return void
     */
    private static function map(array $data, string $key)
    {
        $key = strtoupper($key);

        return $data[$key];
    }

    /**
     * Map type
     *
     * @param string $type
     * @return string
     */
    public static function mapType(string $type)
    {
        $types = [
            Order::TYPE_JACKET      => 'Jaket',
            Order::TYPE_T_SHIRT     => 'Baju',
            Order::TYPE_SHIRT       => 'Kemeja',
            Order::TYPE_SWEATER     => 'Sweater',
            Order::TYPE_OTHER       => 'Lainnya'
        ];

        return self::map($types, $type);
    }

    /**
     * Map dp paid
     *
     * @param integer $isDpPaid
     * @return string
     */
    public static function mapDpPaid(int $isDpPaid)
    {
        $isDpPaids = [
            Order::DP_PAID_TRUE     => 'Sudah DP',
            Order::DP_PAID_FALSE    => 'Belum DP'
        ];

        return self::map($isDpPaids, $isDpPaid);
    }

    /**
     * Map dp paid
     *
     * @param integer $isPaidOff
     * @return string
     */
    public static function mapPaidOff(int $isPaidOff)
    {
        $isPaidOffs = [
            Order::PAID_OFF_TRUE     => 'Sudah Lunas',
            Order::PAID_OFF_FALSE    => 'Belum Lunas'
        ];

        return self::map($isPaidOffs, $isPaidOff);
    }
}