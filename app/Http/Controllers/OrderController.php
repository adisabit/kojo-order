<?php

namespace App\Http\Controllers;

use App\Exceptions\DataNotFoundException;
use App\Helper\OrderHelper;
use App\Library\Response;
use App\Model\ClothingMaterial;
use App\Model\ClothingType;
use App\Model\Invoice;
use App\Model\Order;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class OrderController extends Controller
{
    /**
     * Place an order
     *
     * @param Request $request
     * @return void
     */
    public function order(Request $request)
    {
        $data = $request->all();
        $data['address'] = "{$request->address_street} Kelurahan {$request->address_village} Kecamatan {$request->address_kecamatan}, {$request->address_city}, {$request->address_province} {$request->address_postal_code}";
        $data['order_ref'] = OrderHelper::generateOrderRef();
        $data['order_code'] = generateOrderCode();
        $data['order_time'] = date("Y-m-d H:i:s");
        $data['status'] = Order::STATUS_NEW;

        $total = 0;
        foreach ($data['detail'] as $detail) {
            $total += $detail['quantity'];
        }

        $data['total'] = $total;
        $data['detail'] = json_encode($data['detail']);
        $data['order_detail'] = json_encode($data['order_detail']);
        
        $clothingType = ClothingType::where('code', $request->type)->first();
        $clothingMaterial = ClothingMaterial::where('code', $request->material)->first();

        $data['order_name'] = sprintf("%s %s - %s - %s", $clothingType->name ?? $request->type,
                                                         $clothingMaterial->name ?? $request->material,
                                                         $request->name,
                                                         $request->phone_number);
        
        DB::beginTransaction();

        try {
            // create order
            $order = Order::create($data);

            $order->detail = json_decode($order->detail);
            $order->order_detail = json_decode($order->order_detail);

            // create invoice
            $invoiceData = [
                'order_id' => $order->id,
                'code' => generateInvoiceCode(),
                'name' => $order->name,
                'address' => $order->address,
                'phone_number' => $order->phone_number,
                'type' => $clothingType->name ?? $order->type,
                'material' => $clothingMaterial->name ?? $order->material,
                'total' => $order->total,
                'order_code' => $order->order_code,
                'order_status' => ucwords(join(' ', explode('_', strtolower($order->order_status)))),
                'enter_date' => date("Y-m-d", strtotime($order->order_time)),
                'institution' => $order->institution,
            ];

            $orderDetail = [];
            foreach ($order->detail as $item) {
                $orderDetail[] = [
                    'type' => $clothingType->name ?? $order->type,
                    'material' => $clothingMaterial->name ?? $order->material,
                    'sleeve' => $item->type,
                    'size' => $item->size,
                    'quantity' => $item->quantity,
                    'price' => NULL,
                ];
            }

            $invoiceData['detail'] = json_encode($orderDetail);

            Invoice::create($invoiceData);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            throw $e;
        }

        // create invoice
        return Response::instance()
            ->json($order)
            ->success(201);
    }

    /**
     * Get order details
     *
     * @param $orderRef
     * @return void
     */
    public function detail($code)
    {
        $order = Order::where('order_code', $code)->first();

        if (!$order) {
            throw new DataNotFoundException('Order tidak ditemukan!');
        }

        $clothingType = ClothingType::where('code', $order->type)->first();
        $clothingType = $clothingType ? $clothingType->name : $order->type;
        
        $clothingMaterial = ClothingMaterial::where('code', $order->material)->first();
        $clothingMaterial = $clothingMaterial ? $clothingMaterial->name : $order->material;

        $order->type = $clothingType;
        $order->material = $clothingMaterial;
        
        $order['detail'] = json_decode($order['detail']);
        $order['order_detail'] = json_decode($order['order_detail']);

        $invoice = Invoice::where('order_id', $order->id)->first();
        if ($invoice && $invoice->total_price != null) {
            $order['invoice_url'] = route('order.invoice.download', ['code' => $code]);
            $order['total_price'] = $invoice['total_price'];
            $order['total_dp'] = $invoice['total_dp'];
        } else {
            $order['invoice_url'] = NULL;
            $order['total_price'] = NULL;
            $order['total_dp'] = NULL;
        }

        return Response::instance()
            ->json($order)
            ->success();
    }

    /**
     * Upload image
     *
     * @param Request $request
     * @param string $fileParamName
     * @param string $destination
     * @return void
     */
    private function upload(Request $request,
                            string $fileParamName,
                            string $destination)
    {
        $file = $request->file($fileParamName);

        $mimeType = $file->getMimeType();
        $ext = explode('/', $mimeType)[1];
        $fileName = env('APP_ENV') . '/' . $destination . '/' . md5(microtime(true)) . '.' . $ext;

        $storage = app('firebase.storage');
        $bucket = $storage->getBucket();

        $bucket->upload(file_get_contents($file->getPathName()), [
            'name'      => $fileName,
            'resumable' => true
        ]);

        $url = "https://firebasestorage.googleapis.com/v0/b/kojo-cloth.appspot.com/o/" . rawurlencode($fileName);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);

        curl_close($ch);

        if ($result === 'FALSE') {
            throw new Exception(sprintf('cURL error occured: %s (%s)', curl_error($ch), curl_errno($ch)));
        }

        $responseJson = json_decode($result, true);

        $url .= '?alt=media&token=' . $responseJson['downloadTokens'];

        return $url;
    }

    /**
     * Upload design
     *
     * @param Request $request
     * @return void
     */
    public function uploadDesign(Request $request)
    {
        $url = $this->upload($request, 'design', 'design');

        return Response::instance()
            ->json([
                'url' => $url
            ])->success();
    }

    /**
     * Pay dp
     *
     * @param $code
     * @param Request $request
     * @return void
     */
    public function payDp($code, Request $request)
    {
        $invoice = Invoice::where('order_code', $code)->first();

        if (!$invoice) {
            throw new DataNotFoundException('Invoice tidak ditemukan!');
        }

        $url = $this->upload($request, 'dp_proof', 'dp_proof');

        $result = $invoice->update(['dp_proof_url' => $url]);

        return Response::instance()
            ->json(['is_updated' => $result])
            ->success();
    }

    /**
     * Pay off
     *
     * @param $code
     * @param Request $request
     * @return void
     */
    public function payOff($code, Request $request)
    {
        $invoice = Invoice::where('order_code', $code)->first();

        if (!$invoice) {
            throw new DataNotFoundException('Invoice tidak ditemukan!');
        }

        $url = $this->upload($request, 'pay_off_proof', 'pay_off_proof');

        $result = $invoice->update(['paid_off_proof_url' => $url]);

        return Response::instance()
            ->json(['is_updated' => $result])
            ->success();
    }

    public function downloadInvoice($code)
    {
        $invoice = Invoice::where('order_code', $code)->first();

        if (!$invoice) {
            throw new DataNotFoundException('Invoice tidak ditemukan!');
        }

        $invoice->detail = json_decode($invoice->detail);

        // return view('invoice/index', compact('invoice'));
        $view = View::make('invoice.index', compact('invoice'))->render();

        $dompdf = new \Dompdf\Dompdf();
        $dompdf->loadHtml($view, 'utf-8');
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();

        return $dompdf->stream("{$invoice->code}.pdf");
    }
}
