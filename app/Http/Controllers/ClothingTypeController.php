<?php

namespace App\Http\Controllers;

use App\Exceptions\DataNotFoundException;
use App\Library\Response;
use App\Model\ClothingButton;
use App\Model\ClothingJacketType;
use App\Model\ClothingKurRope;
use App\Model\ClothingMaterial;
use App\Model\ClothingPuring;
use App\Model\ClothingScreenPrinting;
use App\Model\ClothingStopper;
use App\Model\ClothingType;
use App\Model\ClothingZipper;

class ClothingTypeController extends Controller
{
    /**
     * Get list of all clothing types
     *
     * @return void
     */
    public function list()
    {
        $clothingType = ClothingType::orderBy('name')->get(['id', 'code', 'name']);

        return Response::instance()
            ->json($clothingType)
            ->success();
    }

    /**
     * Get materials by type
     *
     * @param [type] $typeId
     * @return void
     */
    public function getTypeAttributes($typeId)
    {
        if (strtolower($typeId) == 'other') {
            $result = [
                'materials'         => ClothingMaterial::all(),
                'buttons'           => ClothingButton::all(),
                'kur_ropes'         => ClothingKurRope::all(),
                'purings'           => ClothingPuring::all(),
                'screen_printings'  => ClothingScreenPrinting::all(),
                'stoppers'          => ClothingStopper::all(),
                'zippers'           => ClothingZipper::all(),
            ];
        } else {
            $clothingType = ClothingType::with([
                'materials',
                'buttons',
                'kurRopes',
                'purings',
                'screenPrintings',
                'stoppers',
                'zippers',
            ])->where(['id' => $typeId])->first();

            if (!$clothingType) {
                throw new DataNotFoundException('Jenis pakaian tidak ditemukan!');
            }

            $result = [
                'materials'         => $clothingType->materials,
                'buttons'           => $clothingType->buttons,
                'kur_ropes'         => $clothingType->kurRopes,
                'purings'           => $clothingType->purings,
                'screen_printings'  => $clothingType->screenPrintings,
                'stoppers'          => $clothingType->stoppers,
                'zippers'           => $clothingType->zippers,
            ];

            if ($clothingType->code == 'JACKET') {
                $result['jacket_types'] = ClothingJacketType::all();
            }
        }

        return Response::instance()
            ->json($result)
            ->success();
    }
}
