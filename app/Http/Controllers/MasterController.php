<?php

namespace App\Http\Controllers;

use App\Library\Response;
use App\Model\MasterPostalCode;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class MasterController extends Controller
{
    /**
     * Get provinces
     *
     * @return void
     */
    public function getProvinces()
    {
        $provinces = MasterPostalCode::distinct()
            ->orderBy('province')
            ->get(['province']);

        return Response::instance()
            ->json($provinces)
            ->success();
    }

    /**
     * Get cities
     *
     * @param Request $request
     * @return void
     */
    public function getCities(Request $request)
    {
        $province = $request->query('province');

        if (!$province) {
            throw new HttpException(400, 'Provinsi harus diisi!');
        }

        $cities = MasterPostalCode::distinct()
            ->where('province', $province)
            ->orderBy('city')
            ->get(['city']);

        return Response::instance()
            ->json($cities)
            ->success();
    }

    /**
     * Get list of kecamatan
     *
     * @param Request $request
     * @return void
     */
    public function getKecamatan(Request $request)
    {
        $province = $request->query('province');

        if (!$province) {
            throw new HttpException(400, 'Provinsi harus diisi!');
        }
        
        $city = $request->query('city');

        if (!$city) {
            throw new HttpException(400, 'Kota harus diisi!');
        }

        $kecamatanList = MasterPostalCode::distinct()
            ->where([
                'province' => $province,
                'city' => $city,
            ])
            ->orderBy('kecamatan')
            ->get(['kecamatan']);

        return Response::instance()
            ->json($kecamatanList)
            ->success();
    }

    /**
     * Get list of kelurahan
     *
     * @param Request $request
     * @return void
     */
    public function getKelurahan(Request $request)
    {
        $province = $request->query('province');

        if (!$province) {
            throw new HttpException(400, 'Provinsi harus diisi!');
        }
        
        $city = $request->query('city');

        if (!$city) {
            throw new HttpException(400, 'Kota harus diisi!');
        }
        
        $kecamatan = $request->query('kecamatan');

        if (!$kecamatan) {
            throw new HttpException(400, 'Kecamatan harus diisi!');
        }

        $kelurahanList = MasterPostalCode::distinct()
            ->where([
                'province' => $province,
                'city' => $city,
                'kecamatan' => $kecamatan,
            ])
            ->orderBy('kelurahan')
            ->get(['kelurahan']);

        return Response::instance()
            ->json($kelurahanList)
            ->success();
    }

    /**
     * Get postal codes
     *
     * @param Request $request
     * @return void
     */
    public function getPostalCodes(Request $request)
    {
        $province = $request->query('province');

        if (!$province) {
            throw new HttpException(400, 'Provinsi harus diisi!');
        }
        
        $city = $request->query('city');

        if (!$city) {
            throw new HttpException(400, 'Kota harus diisi!');
        }
        
        $kecamatan = $request->query('kecamatan');

        if (!$kecamatan) {
            throw new HttpException(400, 'Kecamatan harus diisi!');
        }
        
        $kelurahan = $request->query('kelurahan');

        if (!$kelurahan) {
            throw new HttpException(400, 'Kelurahan harus diisi!');
        }

        $postalCodes = MasterPostalCode::distinct()
            ->where([
                'province' => $province,
                'city' => $city,
                'kecamatan' => $kecamatan,
                'kelurahan' => $kelurahan,
            ])
            ->orderBy('postal_code')
            ->get(['postal_code']);

        return Response::instance()
            ->json($postalCodes)
            ->success();
    }
}
