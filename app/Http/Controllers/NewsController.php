<?php

namespace App\Http\Controllers;

use App\Exceptions\DataNotFoundException;
use App\Library\Response;
use App\Model\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Get latest news
     *
     * @return void
     */
    public function list(Request $request)
    {
        $numOfNews = $request->num_of_news ?? env('NUM_OF_NEWS', 5);
        
        $news = News::where('status', News::STATUS_PUBLISHED)
            ->orderBy('published_at', 'desc')
            ->limit($numOfNews)
            ->get(['featured_image', 'title', 'slug', 'body', 'published_at']);

        foreach ($news as $item) {
            $body = strip_tags($item->body);
            $body = explode(' ', $body);
            $body = array_splice($body, 0, 30);
            $body = join(' ', $body) . '...';

            $item->body = $body;
        }

        return Response::instance()
            ->json($news)
            ->success();
    }

    /**
     * Get news by slug
     *
     * @param string $slug
     * @return void
     */
    public function getBySlug($slug)
    {
        $news = News::where('slug', $slug)->first(['title', 'featured_image', 'slug', 'body', 'published_at']);

        if (!$news) {
            throw new DataNotFoundException('Berita tidak ditemukan!', 204);
        }

        return Response::instance()
            ->json($news)
            ->success();
    }
}