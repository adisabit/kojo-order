<?php

namespace App\Http\Middleware;

use Closure;

class CorsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $headers = [
            'Access-Control-Allow-Origin'      => config('cors.allow_origins'),
            'Access-Control-Allow-Methods'     => config('cors.allow_methods'),
            'Access-Control-Allow-Credentials' => config('cors.allow_credentials'),
            'Access-Control-Max-Age'           => config('cors.max_age'),
            'Access-Control-Allow-Headers'     => config('cors.allow_headers')
        ];

        if ($request->isMethod('OPTIONS'))
        {
            return response()->json('{"method":"OPTIONS"}', 200, $headers);
        }

        $response = $next($request);

        foreach($headers as $key => $value)
        {
            $response->header($key, $value);
        }

        return $response;
    }
}
