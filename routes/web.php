<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'order'], function () use ($router) {
    $router->post('/', ['uses' => 'OrderController@order']);
    $router->get('/{code}', ['uses' => 'OrderController@detail']);
    $router->post('/design/upload', ['uses' => 'OrderController@uploadDesign']);
    $router->post('/{code}/pay_dp', ['uses' => 'OrderController@payDp']);
    $router->post('/{code}/pay_off', ['uses' => 'OrderController@payOff']);
    $router->get('/{code}/invoice/download', [
        'as' => 'order.invoice.download',
        'uses' => 'OrderController@downloadInvoice'
    ]);
});

$router->group(['prefix' => 'clothing'], function () use ($router) {
    $router->get('/type', ['uses' => 'ClothingTypeController@list']);
    $router->get('/type/{typeId}/attributes', ['uses' => 'ClothingTypeController@getTypeAttributes']);
});

$router->group(['prefix' => 'master'], function () use ($router) {
    $router->get('/provinces', ['uses' => 'MasterController@getProvinces']);
    $router->get('/cities', ['uses' => 'MasterController@getCities']);
    $router->get('/kecamatan', ['uses' => 'MasterController@getKecamatan']);
    $router->get('/kelurahan', ['uses' => 'MasterController@getKelurahan']);
    $router->get('/postal_codes', ['uses' => 'MasterController@getPostalCodes']);
});

$router->group(['prefix' => 'news'], function () use ($router) {
    $router->get('/', ['uses' => 'NewsController@list']);
    $router->get('/{slug}', ['uses' => 'NewsController@getBySlug']);
});